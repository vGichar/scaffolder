using System;
using MongoRepository;

namespace _TEMPLATE_.Core
{
    public abstract class BaseEntity : IEntity<Guid>
    {
        public virtual Guid Id { get; set; }
        
        public virtual DateTime ModifiedAt { get; set; }

        public virtual DateTime CreatedAt { get; set; }
    }
}
