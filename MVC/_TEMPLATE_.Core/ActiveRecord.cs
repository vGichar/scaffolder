using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using _TEMPLATE_.Core;
using System.Reflection;

namespace _TEMPLATE_.Core
{
    public abstract class ActiveRecord<T> : BaseEntity where T : ActiveRecord<T>
    {
        private static IRepository<T> Repository => ServiceLocator.Current.GetInstance<IRepository<T>>();

        public static IList<T> Query(Expression<Func<T, bool>> predicate = null, int skip = 0, int take = int.MaxValue)
        {
            return Repository.Query(predicate, skip, take);
        }

        public static T Get(Guid id)
        {
            return Repository.Get(id) as T;
        }

        public static void Delete(Guid id)
        {
            Repository.Delete(id);
        }

        public static void Truncate()
        {
            Repository.Truncate();
        }

        public virtual void Delete()
        {
            Repository.Delete(this as T);
        }

        public virtual void Save()
        {
            Repository.Save(this as T);
        }
    }
}
