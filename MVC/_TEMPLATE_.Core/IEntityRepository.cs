using _TEMPLATE_.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Core
{
    public interface IEntityRepository<T> where T : Entity<T>
    {
        void Truncate();

        IList<T> Query(Expression<Func<T, bool>> predicate = null, int skip = 0, int take = int.MaxValue);

        T Get(Guid id);

        void Save(T obj);

        void Delete(Guid id);

        void Delete(T obj);
    }
}
