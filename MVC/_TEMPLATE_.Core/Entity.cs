using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;
using MongoRepository;

namespace _TEMPLATE_.Core
{
    public abstract class Entity<T> : BaseEntity where T : Entity<T>
    {
        private static IEntityRepository<T> Repository => ServiceLocator.Current.GetInstance<IEntityRepository<T>>();
    }
}
