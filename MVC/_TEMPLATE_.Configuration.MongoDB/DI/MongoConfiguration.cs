using Autofac;
using MongoDB.Driver;
using MongoRepository;
using _TEMPLATE_.Configuration.Base;
using _TEMPLATE_.Configuration.Mongo.Repository;

namespace _TEMPLATE_.Configuration.Mongo.DI
{
    public class MongoConfiguration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterGeneric(typeof(MongoRepository<,>)).UsingConstructor(typeof(MongoUrl)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(MongoDBRepository<>)).As(typeof(Core.IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(MongoDBEntityRepository<>)).As(typeof(Core.IEntityRepository<>)).InstancePerLifetimeScope();
            builder.Register((container) =>
            {
                var settings = container.Resolve<IAppSettings>();
                return new MongoUrl(settings.MongoDBConnectionString);
            }).InstancePerLifetimeScope();
        }
    }
}
