using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoRepository;
using _TEMPLATE_.Configuration.Mongo.Repository;
using _TEMPLATE_.Core;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Configuration.Mongo.Repository
{    
    public class MongoDBEntityRepository<T> : Core.IEntityRepository<T> where T : Entity<T>
    {

        public void Delete(T obj)
        {
        }

        public void Delete(Guid id)
        {
        }

        public T Get(Guid id)
        {
            return null;
        }

        public IList<T> Query(Expression<Func<T, bool>> predicate = null, int skip = 0, int take = int.MaxValue)
        {
            return null;
        }

        public void Save(T obj)
        {
            obj.ModifiedAt = DateTime.UtcNow;
            if (obj.Id == Guid.Empty)
            {
                obj.CreatedAt = DateTime.UtcNow;
            }
        }

        public void Truncate()
        {
        }
    }
}
