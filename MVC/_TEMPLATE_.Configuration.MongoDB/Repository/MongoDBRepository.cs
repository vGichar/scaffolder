using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoRepository;
using _TEMPLATE_.Configuration.Mongo.Repository;
using _TEMPLATE_.Core;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Configuration.Mongo.Repository
{    
    public class MongoDBRepository<T> : Core.IRepository<T> where T : ActiveRecord<T>
    {
        protected MongoRepository<T, Guid> _repository;

        public MongoDBRepository(MongoUrl url)
        {
            _repository = new MongoRepository<T, Guid>(url, typeof(T).FullName);
        }

        public void Delete(T obj)
        {
            _repository.Delete(obj);
        }

        public void Delete(Guid id)
        {
            _repository.Delete(id);
        }

        public T Get(Guid id)
        {
            return _repository.GetById(id);
        }

        public IList<T> Query(Expression<Func<T, bool>> predicate = null, int skip = 0, int take = int.MaxValue)
        {
            if(predicate == null)
            {
                return _repository.Skip(skip).Take(take).ToList();
            }
            return _repository.Where(predicate).Skip(skip).Take(take).ToList();
        }

        public void Save(T obj)
        {
            obj.ModifiedAt = DateTime.UtcNow;
            if (obj.Id == Guid.Empty)
            {
                obj.CreatedAt = DateTime.UtcNow;
                _repository.Add(obj);
            }
            else
            {
                _repository.Update(obj);
            }
        }

        public void Truncate()
        {
            _repository.DeleteAll();
        }
    }
}
