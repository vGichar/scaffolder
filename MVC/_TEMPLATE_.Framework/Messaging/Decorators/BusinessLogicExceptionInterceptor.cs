using _TEMPLATE_.Services;

namespace _TEMPLATE_.Framework.Messaging.Decorators
{
    public class BusinessLogicExceptionInterceptor : IDispatcher
    {
        private IDispatcher _inner;

        public BusinessLogicExceptionInterceptor(IDispatcher inner)
        {
            _inner = inner;
        }

        public Result<T> Dispatch<T>(BaseRequest<T> request) where T : BaseResponse
        {
            Result<T> result = new Result<T>();
            try
            {
                result = _inner.Dispatch(request);
            }
            catch (CoreException ex)
            {
                result.StatusCode = System.Net.HttpStatusCode.Conflict;
                result.Exception = ex;
            }
            return result;
        }
    }
}
