using _TEMPLATE_.Core.Infrastructure;
using _TEMPLATE_.Framework.Logging;
using _TEMPLATE_.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Framework.Messaging.Decorators
{
    public class UnhandledExceptionLogger : IDispatcher
    {
        private IDispatcher _inner;
        private ILogger _logger;

        public UnhandledExceptionLogger(IDispatcher inner,
            ILogger logger)
        {
            _inner = inner;
            _logger = logger;
        }

        public Result<T> Dispatch<T>(BaseRequest<T> request) where T : BaseResponse
        {
            Result<T> result = new Result<T>();
            try
            {
                result = _inner.Dispatch(request);
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.StatusCode = System.Net.HttpStatusCode.Conflict;
                _logger.Error(result.Exception);
            }
            return result;
        }
    }
}
