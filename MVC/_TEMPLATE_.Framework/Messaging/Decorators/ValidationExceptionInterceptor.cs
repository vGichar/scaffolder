using _TEMPLATE_.Core.Infrastructure;
using _TEMPLATE_.Framework.Logging;
using _TEMPLATE_.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _TEMPLATE_.Framework.Messaging.Exceptions;

namespace _TEMPLATE_.Framework.Messaging.Decorators
{
    public class ValidationExceptionInterceptor : IDispatcher
    {
        private IDispatcher _inner;

        public ValidationExceptionInterceptor(IDispatcher inner)
        {
            _inner = inner;
        }

        public Result<T> Dispatch<T>(BaseRequest<T> request) where T : BaseResponse
        {
            Result<T> result = new Result<T>();
            try
            {
                result = _inner.Dispatch(request);
            }
            catch (ValidationException ex)
            {
                result.StatusCode = System.Net.HttpStatusCode.Conflict;
                result.Exception = ex;
            }
            return result;
        }
    }
}
