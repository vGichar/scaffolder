using Autofac;
using _TEMPLATE_.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using _TEMPLATE_.Framework.Messaging.Exceptions;

namespace _TEMPLATE_.Framework.Messaging
{
    public class Dispatcher : IDispatcher
    {
        private static Type[] _handlerTypes { get; } = typeof(BaseResponse).Assembly.GetHandlerTypes();
        private static Type[] _validatorTypes { get; } = typeof(BaseResponse).Assembly.GetValidatorTypes();
        private static Type[] _requestTypes { get; } = typeof(BaseResponse).Assembly.GetRequestTypes();

        private IComponentContext _container;

        public Dispatcher(IComponentContext container)
        {
            _container = container;
        }

        public Result<T> Dispatch<T>(BaseRequest<T> request) where T : BaseResponse
        {
            var responseType = typeof(T);
            var requestType = request.GetType();
            var handlerType = GetTypeInHaystack(_handlerTypes, responseType, requestType);
            var validatorType = GetTypeInHaystack(_validatorTypes, responseType, requestType);

            var result = new Result<T>();
            var handler = _container.Resolve(handlerType.BaseType);
            Validate(validatorType, request);

            result.Response = Execute(handler, request);
            result.Exception = null;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            return result;
        }

        public void Validate<T>(Type validatorType, BaseRequest<T> request) where T : BaseResponse
        {
            if (validatorType != null)
            {
                var validator = _container.Resolve(validatorType.BaseType);
                var results = validator?.GetType().GetMethod("Validate", new Type[] { request.GetType() }).Invoke(validator, new object[] { request }) as ValidationResult;
                if (!results.IsValid)
                {
                    throw new ValidationException(results.Errors.Select(x =>
                    {
                        var exception = new Exception(x.ErrorMessage);
                        exception.Source = "FluentValidator";
                        exception.Data["Property"] = x.PropertyName;
                        return exception;
                    }));
                }
            }
        }

        public T Execute<T>(object handler, BaseRequest<T> request) where T : BaseResponse
        {
            try
            {
                return handler.GetType().GetMethod("Handle").Invoke(handler, new object[] { request }) as T;
            }catch(Exception ex)
            {
                throw ex.InnerException;
            }
        }

        private Type GetTypeInHaystack(Type[] searchTypes, Type responseType, Type requestType)
        {
            return searchTypes
                .Where(x => x.BaseType.GetGenericArguments().Any(y => y == responseType) && x.BaseType.GetGenericArguments().Any(y => y == requestType))
                .SingleOrDefault();
        }
    }
}
