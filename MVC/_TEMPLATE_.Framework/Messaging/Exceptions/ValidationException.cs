using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Framework.Messaging.Exceptions
{
    public class ValidationException : AggregateException
    {
        public ValidationException(IEnumerable<Exception> innerExceptions) : base(innerExceptions)
        {
        }
    }
}
