using Autofac;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Linq;
using _TEMPLATE_.Configuration.Base;
using _TEMPLATE_.Configuration.DI;
using _TEMPLATE_.Framework.Messaging;
using _TEMPLATE_.Services;

namespace _TEMPLATE_.UnitTests
{
    public abstract class BaseTest
    {
        private static void BootDI()
        {
            ConfigurationBootstraper.Load(new ContainerBuilder(), new AppSettings());
        }

        public BaseTest()
        {
            BootDI();
        }

        protected Result<T> Dispatch<T>(BaseRequest<T> request) where T : BaseResponse
        {
            var result = ServiceLocator.Current.GetInstance<IDispatcher>().Dispatch(request);
            return result;
        }
    }
}
