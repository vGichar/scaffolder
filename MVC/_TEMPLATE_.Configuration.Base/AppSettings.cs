using _TEMPLATE_.Configuration.Base.Enums;
using System.Configuration;
using System;

namespace _TEMPLATE_.Configuration.Base
{
    public class AppSettings : IAppSettings
    {
        public string MsSqlConnectionString => ConfigurationManager.AppSettings["MsSqlConnectionString"];

        public string MySqlConnectionString => ConfigurationManager.AppSettings["MySqlConnectionString"];

        public string PostgreSqlConnectionString => ConfigurationManager.AppSettings["PostgreSqlConnectionString"];

        public string MongoDBConnectionString => ConfigurationManager.AppSettings["MongoDBConnectionString"];

        public DatabaseType Database => DatabaseTypeHelper.FromString(ConfigurationManager.AppSettings["Database"]);

        public bool IsDebug => bool.Parse(ConfigurationManager.AppSettings["IsDebug"]);

        public bool InMaintenanceMode => bool.Parse(ConfigurationManager.AppSettings["InMaintenanceMode"]);
    }
}
