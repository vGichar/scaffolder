using _TEMPLATE_.Configuration.Base.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Configuration.Base
{
    public interface IAppSettings
    {
        string MsSqlConnectionString { get; }

        string MySqlConnectionString { get; }

        string PostgreSqlConnectionString { get; }

        string MongoDBConnectionString { get; }

        DatabaseType Database { get; }

        bool IsDebug { get; }

        bool InMaintenanceMode { get; }
    }
}
