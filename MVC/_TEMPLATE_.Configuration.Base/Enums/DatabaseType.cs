using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Configuration.Base.Enums
{
    public enum DatabaseType
    {
        MySQL, MsSQL, PostgreSQL, MongoDB, SqLite
    }

    public static class DatabaseTypeHelper
    {
        public static DatabaseType FromString(string dbType)
        {
            switch (dbType.ToLowerInvariant())
            {
                case "mysql":
                    return DatabaseType.MySQL;
                case "sqlite":
                    return DatabaseType.SqLite;
                case "mssql":
                    return DatabaseType.MsSQL;
                case "postgresql":
                    return DatabaseType.PostgreSQL;
                case "mongodb":
                    return DatabaseType.MongoDB;
                default:
                    return DatabaseType.MySQL;
            };
        }
    }
}
