using Autofac;
using Autofac.Extras.CommonServiceLocator;
using Microsoft.Practices.ServiceLocation;
using _TEMPLATE_.Configuration.Base;
using _TEMPLATE_.Configuration.Mongo.DI;
using _TEMPLATE_.Configuration.NHibernateORM.DI;

namespace _TEMPLATE_.Configuration.DI
{
    public static class ConfigurationBootstraper
    {
        public static IContainer Load(ContainerBuilder builder, IAppSettings settings)
        {
            builder.RegisterInstance(settings);
            builder.RegisterAssemblyModules(typeof(ConfigurationBootstraper).Assembly);

            if (settings.Database == Base.Enums.DatabaseType.MySQL
                || settings.Database == Base.Enums.DatabaseType.PostgreSQL
                || settings.Database == Base.Enums.DatabaseType.MsSQL)
            {
                builder.RegisterAssemblyModules(typeof(NHibernateConfiguration).Assembly);
            }
            else if (settings.Database == Base.Enums.DatabaseType.MongoDB)
            {
                builder.RegisterAssemblyModules(typeof(MongoConfiguration).Assembly);
            }

            var container = builder.Build();

            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));

            return container;
        }
    }
}
