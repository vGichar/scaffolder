using Autofac;
using _TEMPLATE_.Framework.Logging;
using _TEMPLATE_.Framework.Messaging;
using _TEMPLATE_.Framework.Messaging.Decorators;
using _TEMPLATE_.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Configuration.DI.Framework.Messaging
{
    public class MessagingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Dispatcher>().Named("dispatcher", typeof(IDispatcher)).InstancePerLifetimeScope();

            builder
                .RegisterAssemblyTypes(typeof(BaseHandler<,>).Assembly)
                .Where(x => x.IsClosedTypeOf(typeof(BaseHandler<,>)))
                .As(x => x.BaseType)
                .InstancePerLifetimeScope();
            builder
                .RegisterAssemblyTypes(typeof(BaseValidator<,>).Assembly)
                .Where(x => x.IsClosedTypeOf(typeof(BaseValidator<,>)))
                .As(x => x.BaseType)
                .InstancePerLifetimeScope();

            // Decorator
            builder.RegisterDecorator<IDispatcher>((x, inner) => new ValidationExceptionInterceptor(inner), "dispatcher").Named<IDispatcher>("validatedDispatcher").InstancePerLifetimeScope();
            builder.RegisterDecorator<IDispatcher>((x, inner) => new BusinessLogicExceptionInterceptor(inner), "validatedDispatcher").Named<IDispatcher>("businessDispatcher").InstancePerLifetimeScope();
            builder.RegisterDecorator<IDispatcher>((x, inner) => new UnhandledExceptionLogger(inner, x.Resolve<ILogger>()), "businessDispatcher").InstancePerLifetimeScope();
        }
    }
}
