
var gulp = require("gulp");
var rename = require("gulp-regex-rename");
var replace = require("gulp-replace");
var clean = require("gulp-clean");
var runSequence = require('run-sequence');
var through = require('through2')

var fromName = "";

gulp.task("settings", function(){
	runSequence("get-name", "copy-settings");
});

gulp.task("copy-settings", function(){
	gulp.src(['./AppSettings.Debug.config', 'AppSettings.Release.config'])
		.pipe(gulp.dest(fromName + ".Web"))
		.pipe(gulp.dest(fromName + ".Console"))
		.pipe(gulp.dest(fromName + ".UnitTests"))
});

gulp.task("template", function(){
	runSequence("get-name", "rename", "clean");
})

gulp.task("rename", function(){
	return gulp.src(["./**/*", "!./**/node_modules/**", "!./**/bower_components/**"])
		.pipe(gulp.dest("./dest-temp"))
		.pipe(replace(new RegExp(fromName, "g"), "_TEMPLATE_", {skipBinary: true}))
		.pipe(rename(new RegExp(fromName, "g"), "_TEMPLATE_"))
		.pipe(gulp.dest("./_TEMPLATE_"))
});

gulp.task("clean", function(){
	return gulp.src("./dest-temp")
		.pipe(clean());
});


gulp.task("get-name", function(){
	return gulp.src("./*.sln")
		.pipe(through.obj(function (chunk, enc, cb) {
			var pathParts = chunk.path.split('\\');
			var fileName = pathParts[pathParts.length - 1];
			fileName = fileName.replace(".sln", "");
			fromName = fileName;
			cb(null, chunk);
		}));
});
