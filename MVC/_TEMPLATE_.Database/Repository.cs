using QSeed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using _TEMPLATE_.Core;
using QSeed.Repository;
using Microsoft.Practices.ServiceLocation;

namespace _TEMPLATE_.DatabaseSeeders
{
    public class Repository<T> : QSeed.Repository.IRepository<T> where T : ActiveRecord<T>
    {
        private Core.IRepository<T> _repo => ServiceLocator.Current.GetInstance<Core.IRepository<T>>();
        
        public void Save(T obj)
        {
            _repo.Save(obj);
        }

        public void ClearData()
        {
            foreach(var obj in _repo.Query())
            {
                obj.Delete();
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> expression = null)
        {
            return _repo.Query(expression);
        }

        public void Remove(T obj)
        {
            _repo.Delete(obj);
        }
    }
}
