using QSeed;
using QSeed.SeederTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QSeed.Config;
using _TEMPLATE_.Configuration.Base;
using Microsoft.Practices.ServiceLocation;

namespace _TEMPLATE_.DatabaseSeeders
{
    public class DatabaseSeeder : MasterSeeder
    {
        private IAppSettings _settings => ServiceLocator.Current.GetInstance<IAppSettings>();

        public override void Run()
        {
            if (_settings.IsDebug)
            {
            }
        }
    }
}
