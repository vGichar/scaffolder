using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.DatabaseSeeders.Extensions
{
    public static class LinqExtensions
    {
        public static T Random<T>(this IEnumerable<T> array)
        {
            var rand = new Random().Next(array.Count());

            return array.ElementAt(rand);
        }
    }
}
