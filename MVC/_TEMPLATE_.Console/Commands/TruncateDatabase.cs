using QMand.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QMand.Commands.Definition;
using _TEMPLATE_.Core;

namespace _TEMPLATE_.Console.Commands
{
    public class TruncateDatabase : ConsoleCommand
    {
        public override string Description => "Empties the entire database";

        public override string Name => "trunc";

        public override List<CommandParameter> ParametersDefinition => new List<CommandParameter>();

        public override void Run()
        {
            var entityTypes = typeof(BaseEntity).Assembly.GetExportedTypes().Where(x => x.IsSubclassOf(typeof(BaseEntity)));
            foreach (var entityType in entityTypes)
            {
                try
                {
                    entityType.BaseType.GetMethod("Truncate").Invoke(null, null);
                }
                catch
                {

                }
            }
        }
    }
}
