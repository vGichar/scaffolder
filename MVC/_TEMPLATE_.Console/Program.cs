using Autofac;
using FluentNHibernate.Automapping;
using Microsoft.Practices.ServiceLocation;
using _TEMPLATE_.Configuration.Base;
using _TEMPLATE_.Configuration.DI;
using QMand;
using _TEMPLATE_.Configuration.Base.Enums;
using System;

namespace _TEMPLATE_.Console
{
    public class Program
    {
        private static void BootDI()
        {
            ConfigurationBootstraper.Load(new ContainerBuilder(), new AppSettings());
        }

        static void Main(string[] args)
        {
            BootDI();

            var marshal = new CommandMarshal();
            marshal.RegisterCommandAssembly(typeof(Program).Assembly);

            if (args.Length > 0)
            {
                foreach (var line in args)
                {
                    marshal.ExecuteCommandString(line);
                }
            }
            else
            {
                while (true)
                {
                    System.Console.Write(">");
                    var line = System.Console.ReadLine();
                    marshal.ExecuteCommandString(line);
                }
            }
        }
    }
}
