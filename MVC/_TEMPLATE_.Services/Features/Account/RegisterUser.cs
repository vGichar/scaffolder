using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using _TEMPLATE_.Core.Features.Account;
using _TEMPLATE_.Services.Helpers;

namespace _TEMPLATE_.Services.Features.Account
{
    public class RegisterUser
    {
        public class Request : BaseRequest<Response>
        {
            public string Email { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class Response : BaseResponse
        {
            public Guid Id { get; set; }
        }

        public class Validator : BaseValidator<Response, Request>
        {
            public Validator()
            {
                RuleFor(x => x.Email).NotEmpty().EmailAddress();
                RuleFor(x => x.Password).NotEmpty();
                RuleFor(x => x.Username).NotEmpty();
            }
        }

        public class Handler : BaseHandler<Response, Request>
        {
            public override Response Handle(Request request)
            {
                var userExists = User.Query(x => x.Email == request.Email || x.Username == request.Username).Any();

                if (userExists)
                {
                    throw new CoreException("User with such email or username alread exists");
                }

                var salt = PasswordHasher.GenearteSalt();
                var user = new User {
                    Email = request.Email,
                    Username = request.Username,
                    Salt = salt,
                    Password = PasswordHasher.HashPassword(request.Password, salt)
                };

                user.Save();

                return new Response
                {
                    Id = user.Id
                };
            }
        }
    }
}
