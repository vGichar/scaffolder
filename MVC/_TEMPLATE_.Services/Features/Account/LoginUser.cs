using _TEMPLATE_.Core.Features.Account;
using _TEMPLATE_.Services.Helpers;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Services.Features.Account
{
    public class LoginUser
    {
        public class Request : BaseRequest<Response>
        {
            [DisplayName("Username or Email")]
            public string UsernameOrEmail { get; set; }
            
            public string Password { get; set; }
        }

        public class Response : BaseResponse
        {
            public Guid Id { get; set; }
            public string Email { get; set; }
            public string Username { get; set; }
        }

        public class Validator : BaseValidator<Response, Request>
        {
            public Validator()
            {
                RuleFor(x => x.UsernameOrEmail).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }

        public class Handler : BaseHandler<Response, Request>
        {
            public override Response Handle(Request request)
            {
                var user = User.Query(x => (x.Username == request.UsernameOrEmail || x.Email == request.UsernameOrEmail)).SingleOrDefault();

                if(user != null)
                {
                    var passwordsMatch = PasswordHasher.ComparePasswords(user.Password, PasswordHasher.HashPassword(request.Password, user.Salt));
                    if (passwordsMatch)
                    {
                        return new Response {
                            Id = user.Id,
                            Username = user.Username,
                            Email = user.Email
                        };
                    }
                    else
                    {
                        throw new CoreException("Passwords don't match");
                    }
                }
                else
                {
                    throw new CoreException("User doesn't exist");
                }
            }
        }
    }
}
