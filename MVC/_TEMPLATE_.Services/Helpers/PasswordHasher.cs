using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Services.Helpers
{
    public static class PasswordHasher
    {
        public static string GenearteSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }

        public static string HashPassword(string unsecurePassword, string salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainTextWithSaltBytes = new byte[unsecurePassword.Length + salt.Length];

            for (int i = 0; i < unsecurePassword.Length; i++)
            {
                plainTextWithSaltBytes[i] = (byte)unsecurePassword.ElementAt(i);
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[unsecurePassword.Length + i] = (byte)salt.ElementAt(i);
            }

            return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
        }

        public static bool ComparePasswords(string pass1, string pass2)
        {
            if (pass1.Length != pass2.Length)
            {
                return false;
            }

            for (int i = 0; i < pass1.Length; i++)
            {
                if (pass1.ElementAt(i) != pass2.ElementAt(i))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
