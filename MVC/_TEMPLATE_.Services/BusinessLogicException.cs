using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace _TEMPLATE_.Services
{
    public class CoreException : Exception
    {
        public CoreException(string message) : base(message)
        {
        }
    }
}
