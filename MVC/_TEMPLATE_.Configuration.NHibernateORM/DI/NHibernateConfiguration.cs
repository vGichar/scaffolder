using Autofac;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.IO;
using System.Reflection;
using FluentNHibernate.Conventions.Instances;
using _TEMPLATE_.Core;
using _TEMPLATE_.Configuration.Base;
using FluentNHibernate.Automapping.Alterations;
using System;
using FluentNHibernate.Conventions.Helpers;
using _TEMPLATE_.Configuration.NHibernateORM.Repository;
using NHibernate.Mapping.ByCode;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;

namespace _TEMPLATE_.Configuration.NHibernateORM.DI
{
    public class NHibernateConfiguration : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register((container) =>
            {
                var settings = container.Resolve<IAppSettings>();
                return BuildConfiguration(settings);
            }).As<NHibernate.Cfg.Configuration>().SingleInstance();

            builder.Register((container) =>
            {
                var configuration = container.Resolve<NHibernate.Cfg.Configuration>();
                return BuildSessionFactory(configuration);
            }).As<ISessionFactory>().SingleInstance();

            builder.Register(x =>
            {
                var session = x.Resolve<ISessionFactory>().OpenSession();
                var cfg = x.Resolve<NHibernate.Cfg.Configuration>();

                return session;
            }).As<ISession>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(NHibernateRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(NHibernateEntityRepository<>)).As(typeof(IEntityRepository<>)).InstancePerLifetimeScope();

            builder.Register((container) =>
            {
                var configuration = container.Resolve<NHibernate.Cfg.Configuration>();
                var model = BuildPersistenceModel(configuration);
                model.Configure(configuration);
                return model;
            }).As<AutoPersistenceModel>().SingleInstance().AutoActivate();
        }

        private NHibernate.Cfg.Configuration BuildConfiguration(IAppSettings appSettings)
        {
            var config = Fluently.Configure()
                .Database(SelectDatabaseType(appSettings))
                .ExposeConfiguration(c => c.SetProperty(NHibernate.Cfg.Environment.ReleaseConnections, "on_close"))
                .ExposeConfiguration(c => c.SetProperty(NHibernate.Cfg.Environment.ProxyFactoryFactoryClass, typeof(NHibernate.Bytecode.DefaultProxyFactoryFactory).AssemblyQualifiedName))
                .ExposeConfiguration(c => c.SetProperty(NHibernate.Cfg.Environment.Hbm2ddlAuto, "update"))
                //.ExposeConfiguration(c => c.SetProperty(NHibernate.Cfg.Environment.ShowSql, "true"))
                .BuildConfiguration();

            if (config == null)
                throw new System.Exception("Cannot build NHibernate configuration");

            return config;
        }

        private IPersistenceConfigurer SelectDatabaseType(IAppSettings settings)
        {
            if (settings.Database == Base.Enums.DatabaseType.MySQL)
            {
                return MySQLConfiguration.Standard.ConnectionString(settings.MySqlConnectionString);
            }
            else if (settings.Database == Base.Enums.DatabaseType.SqLite)
            {
                return SQLiteConfiguration.Standard.InMemory();
            }
            else if (settings.Database == Base.Enums.DatabaseType.MsSQL)
            {
                return MsSqlConfiguration.MsSql2012.ConnectionString(settings.MsSqlConnectionString);
            }
            else if (settings.Database == Base.Enums.DatabaseType.PostgreSQL)
            {
                return PostgreSQLConfiguration.PostgreSQL82.ConnectionString(settings.PostgreSqlConnectionString).DefaultSchema("public");
            }

            return null;
        }

        private ISessionFactory BuildSessionFactory(NHibernate.Cfg.Configuration config)
        {
            var sessionFactory = config.BuildSessionFactory();

            if (sessionFactory == null)
                throw new System.Exception("Cannot build NHibernate Session Factory");

            return sessionFactory;
        }

        private AutoPersistenceModel BuildPersistenceModel(NHibernate.Cfg.Configuration config)
        {
            var persistenceModel = AutoMap.AssemblyOf<BaseEntity>();
            persistenceModel.Conventions.Add<StringColumnLengthConvention>();
            persistenceModel.Conventions.Add(DefaultCascade.All());
            persistenceModel.Override(typeof(GuidOverride));
            persistenceModel.UseOverridesFromAssembly(typeof(BaseEntity).Assembly);
            persistenceModel.Configure(config);

            return persistenceModel;
        }
    }

    public class StringColumnLengthConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => x.Type == typeof(string));
        }

        public void Apply(IPropertyInstance instance)
        {
            instance.Length(10000);
        }
    }

    public class GuidOverride : IAutoMappingOverride<BaseEntity>
    {
        public void Override(AutoMapping<BaseEntity> mapping)
        {
            mapping.Id(x => x.Id).GeneratedBy.GuidComb();
        }
    }
}