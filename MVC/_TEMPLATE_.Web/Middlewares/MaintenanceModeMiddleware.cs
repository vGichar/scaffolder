using Microsoft.Owin;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using _TEMPLATE_.Configuration.Base;

namespace _TEMPLATE_.Web.Middlewares
{
    public class MaintenanceModeMiddleware : OwinMiddleware
    {
        public MaintenanceModeMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public async override Task Invoke(IOwinContext context)
        {
            var settings = ServiceLocator.Current.GetInstance<IAppSettings>();

            if (!settings.InMaintenanceMode)
            {
                await Next.Invoke(context);
            }
            else
            {
                var oldUrl = context.Request.Uri.PathAndQuery;
                var newUrl = context.Request.PathBase + "/Home/Maintenance";
                if (oldUrl == newUrl)
                {
                    await Next.Invoke(context);
                }
                else
                {
                    context.Response.Redirect(newUrl);
                }
            }
        }
    }
}