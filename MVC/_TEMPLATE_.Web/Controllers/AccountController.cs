using _TEMPLATE_.Services.Features.Account;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace _TEMPLATE_.Web.Controllers
{
    public class AccountController : BaseController
    {
        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterUser.Request request)
        {
            var result = Dispatch(request);

            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, request.Username));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, result.Response.Id.ToString()));
                claims.Add(new Claim(ClaimTypes.Email, request.Email));
                claims.Add(new Claim(ClaimTypes.Role, "Competitor"));
                claims.Add(new Claim(ClaimTypes.Role, "Admin"));

                IdentitySignin(claims, true);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Register", "Account");
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginUser.Request request)
        {
            var result = Dispatch(request);

            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, result.Response.Username));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, result.Response.Id.ToString()));
                claims.Add(new Claim(ClaimTypes.Email, result.Response.Email));
                claims.Add(new Claim(ClaimTypes.Role, "Competitor"));
                claims.Add(new Claim(ClaimTypes.Role, "Admin"));

                IdentitySignin(claims, true);

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult Logout()
        {
            IdentitySignout();
            return RedirectToAction("Index", "Home");
        }

        private T IdentityExtract<T>() where T : new()
        {
            var principal = User as ClaimsPrincipal;

            var type = typeof(T);
            var properties = type.GetProperties();
            var instance = new T();

            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttributes(typeof(FromClaim), false).SingleOrDefault();
                var claimName = (attribute as FromClaim).Name;

                var value = principal.Claims.SingleOrDefault(x => x.Type == claimName).Value;
                property.SetValue(instance, value);
            }

            return instance;
        }

        private void IdentitySignin(List<Claim> claims, bool isPersistent = false)
        {
            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = isPersistent,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            }, identity);
        }

        private void IdentitySignout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
        public class FromClaim : Attribute
        {
            public string Name { get; set; }
        }
    }
}