using _TEMPLATE_.Framework.Messaging;
using _TEMPLATE_.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using _TEMPLATE_.Framework.Messaging.Exceptions;

namespace _TEMPLATE_.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (TempData["ModelState"] != null && !ModelState.Equals(TempData["ModelState"]))
            {
                ModelState.Merge((ModelStateDictionary)TempData["ModelState"]);
                TempData["ModelState"] = null;
            }
            base.OnActionExecuted(filterContext);
        }

        protected Result<T> Dispatch<T>(BaseRequest<T> request) where T : BaseResponse
        {
            if (!string.IsNullOrEmpty(User?.Identity.Name))
            {
                request.UserId = Guid.Parse(User.Identity.Name);
            }

            var result = DependencyResolver.Current.GetService<IDispatcher>().Dispatch(request);
            if (result.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                if (result.Exception is ValidationException)
                {
                    var exceptions = (result.Exception as ValidationException).InnerExceptions;

                    foreach (var ex in exceptions)
                    {
                        ModelState.AddModelError(ex.Data["Property"] as string, ex.Message);
                    }
                }else if(result.Exception is CoreException)
                {
                    ModelState.AddModelError(string.Empty, result.Exception.Message);
                }else
                {
                    throw result.Exception;
                }
                TempData["ModelState"] = ModelState;
            }
            return result;
        }

        public ActionResult ChangeCulture(string culture)
        {
            var returnUrl = Request.QueryString["ReturnUrl"] as string ?? Request.UrlReferrer?.AbsolutePath ?? "~/";

            var cookie = new HttpCookie("_culture", culture);
            cookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(cookie);

            return Redirect(returnUrl);
        }

        public ActionResult ViewOrJson<T>(Result<T> result = null) where T : BaseResponse
        {
            var isApi = Request.RawUrl.Trim('/').ToLowerInvariant().StartsWith("api");
            if (isApi)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return View(result);
        }

        protected new JsonResult Json(object model)
        {
            return base.Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}