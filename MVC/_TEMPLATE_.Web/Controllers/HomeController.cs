using _TEMPLATE_.i18n;
using System.Web.Mvc;
using _TEMPLATE_.Configuration.Base;
using Microsoft.Practices.ServiceLocation;

namespace _TEMPLATE_.Web.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Maintenance()
        {
            var inMaintenanceMode = ServiceLocator.Current.GetInstance<IAppSettings>().InMaintenanceMode;

            if(inMaintenanceMode)
                return View();
            return RedirectToAction("Index");
        }
    }
}