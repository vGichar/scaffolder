using Autofac;
using Autofac.Integration.Mvc;
using FluentNHibernate.Automapping;
using Microsoft.Practices.ServiceLocation;
using _TEMPLATE_.Configuration;
using _TEMPLATE_.Configuration.Base;
using _TEMPLATE_.Configuration.DI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Security.Principal;
using System.Security.Claims;

namespace _TEMPLATE_.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private void BootDI()
        {
            var container = ConfigurationBootstraper.Load(new ContainerBuilder(), new AppSettings());
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        protected void Application_Start()
        {
            BootDI();
            IAppSettings settings = ServiceLocator.Current.GetInstance<IAppSettings>();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles, settings);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            var name = HttpContext.Current.Request.Cookies.Get("_culture")?.Value as string;

            if (string.IsNullOrEmpty(name))
            {
                return;
            }

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(name);
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                
                string userData = authTicket.UserData;
                
                var userId = authTicket.Name;
                IIdentity identiy = new GenericIdentity(userId);

                var principal = new GenericPrincipal(identiy, userData.Split(','));

                HttpContext.Current.User = principal;
            }
        }
    }
}
