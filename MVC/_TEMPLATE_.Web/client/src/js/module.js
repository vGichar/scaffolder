(function () {
    var viewsDir = "/client/dist/html";
    angular
        .module("app", [
            "ngRoute"
        ])
        .config(config)
        .run(routeCallback);


    config.$inject = ["$routeProvider"];
    function config($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "",
                controller: "",
                controllerAs : "vm"
            })
    }

    routeCallback.$inject = ["$rootScope"];
    function routeCallback($rootScope) {
        $rootScope.$on("$locationChangeSuccess", function (event, next, current) {
            setTimeout(function () {
                $(".ui.dropdown").dropdown();
            }, 0);
        });
    }

})();