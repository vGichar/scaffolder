var gulp = require("gulp");
var sass = require("gulp-sass");
var sassGlob = require('gulp-sass-glob');
var imagemin = require("gulp-imagemin");
var tap = require("gulp-tap");
var concat = require("gulp-concat");
var exec = require('child_process').exec;

gulp.task("watch", ["build"], function () {
    gulp.watch('src/sass/**', ['build-sass']);
    gulp.watch('src/js/**', ['build-javascript', 'build-html']);
    gulp.watch('src/images/**', ['minify-images']);
    // gulp.watch([
    //     '../../**/**',
    //     '!../../**/{bin,bin/**,obj,obj/**,client,client/**}',
    //     '!../../{packages,packages/**}',
    //     '!../../{TestResults,TestResults/**}'
    // ], ['build-solution']);
});

gulp.task("build", ["build-sass", "build-javascript", "build-html", "minify-images"]);

gulp.task("build-sass", function () {
    gulp.src('src/sass/style.scss')
   		.pipe(sassGlob())
   		.pipe(sass())
        .pipe(gulp.dest('dist/css/'));
});

gulp.task("build-javascript", function () {
    gulp.src('src/js/**/*.js')
        .pipe(concat('compiled-js.js'))
        .pipe(gulp.dest('dist/js'))
});

gulp.task("build-html", function () {
    gulp.src('src/js/**/*.html')
        .pipe(gulp.dest('dist/html'))
});

gulp.task("minify-images", function () {
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
});

gulp.task("build-solution", function () {
    var cmd = '"C:\\Program Files (x86)\\MSBuild\\14.0\\Bin\\msbuild" ../../_TEMPLATE_.sln /p:VisualStudioVersion=14.0';
    exec(cmd, function (err, stdout, stderr) {
        //console.log(stdout);
        console.error(stderr);
    });
});