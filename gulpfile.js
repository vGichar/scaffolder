var paths = {
	src : "_SOURCE_",
	dest: "_DESTINATION_"
};
var solutionName = "_SOLUTION_NAME_";
var gulp = require("gulp");
var rename = require("gulp-regex-rename");
var replace = require("gulp-replace");
var runSequence = require('run-sequence');
gulp.task("move", function(){
	return gulp.src(["./" + paths.src + "/**/*", "!./" + paths.src + "/**/node_modules/**", "!./" + paths.src + "/**/bower_components/**"])
		.pipe(gulp.dest("dest-temp"));
});
gulp.task("replace", function(){
	return gulp.src("./dest-temp/**/*")
		.pipe(replace(/_TEMPLATE_/g, solutionName, {skipBinary: true}))
		.pipe(rename(/_TEMPLATE_/g, solutionName))
		.pipe(gulp.dest(paths.dest + "//" + solutionName));
});
gulp.task("build", function(){
	return runSequence("move", "replace");
});


var src = "../Portal";
var dest = "../_TEMPLATE_";
var fromName = "_DESTINATION_";
var toName = "_TEMPLATE_";
gulp.task("rename", function(){
	return gulp.src(["./" + src + "/**/*", "!./" + src + "/**/node_modules/**", "!./" + src + "/**/bower_components/**"])
		.pipe(gulp.dest("dest-temp"))
		.pipe(replace(new RegExp(fromName, "g"), toName, {skipBinary: true}))
		.pipe(rename(new RegExp(fromName, "g"), toName))
		.pipe(gulp.dest(dest));
});
