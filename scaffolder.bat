@echo off
SET currentDir=%cd%
SET command=%1
SET subCommand=%2
SET solutionName=%3
IF "%command%"=="" (
	@echo "Must have at least 2 arguments!"
	EXIT /B 0
)
IF "%subCommand%"=="" (
	@echo "Must have at least 2 arguments!"
	EXIT /B 0
)
IF "%command%"=="new" (
	IF "%solutionName%"=="" (
		@echo "Must have solution name when using subcommand 'new'"
		EXIT /B 0
	)
)
IF "%command%"=="new" (
	cd %~dp0

	@echo "clear..."
	call :revert >nul

	@echo "backup..."
	call copy "gulpfile.js" "gulpfile-temp.js" >nul
	call :find "_SOURCE_" "%subCommand%" "gulpfile.js" >nul
	call :find "_DESTINATION_" "%%currentDir:\=\\%%" "gulpfile.js" >nul
	call :find "_SOLUTION_NAME_" "%%solutionName%%" "gulpfile.js" >nul

	@echo "install..."
	call npm install >nul

	@echo "move..."
	call gulp build >nul

	@echo "clear..."
	call :revert >nul

	@echo "build client..."
	cd "%currentDir%\%solutionName%\%solutionName%.Web\client\"
	call npm install >nul
	call bower install >nul

	cd %currentDir%
	
	@echo(
	@echo "All done!"
	)
)
IF "%command%"=="modify" (
	cd "%~dp0%subCommand%"
	call start explorer.exe "_TEMPLATE_.sln"
	cd "%currentDir%"
)

EXIT /B 0

:find
setlocal enableextensions disabledelayedexpansion
set "search=%1"
set "replace=%2"
set "file=%3"

for /f "delims=" %%i in ('type "%file%" ^& break ^> "%file%" ') do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search%=%replace%!"
    >>"%file%" echo(!line!
    endlocal
)
EXIT /B 0

:revert
IF EXIST "gulpfile-temp.js" (
	call del "gulpfile.js" >nul
	call move "gulpfile-temp.js" "gulpfile.js" >nul
)
IF EXIST "dest-temp" (
	call rmdir /s /q "dest-temp" >nul
)
EXIT /B 0